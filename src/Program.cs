﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
namespace minipl
{
// LL(1) grammar
//
// Program       = Stmts Eof
// Stmts         = (Stmt ";")+
// Stmt          = "var" Ident ":" Type [":=" Expr]
//               = "for" Ident "in" Expr ".." Expr "do" Stmts "end" "for"
//               = "read" Ident
//               = "print" Expr
//               = "assert" "(" Expr ")"
//               = Ident ":=" Expr
// Expr          = "!" Opnd
//               = Opnd [BinOp Opnd]
// Opnd          = IntLiteral
//               = StringLiteral
//               = Ident
//               = "(" Expr ")"
// BinOp         = "+" | "-" | "*" | "/" | "<" | "=" | "&"
// Type          = "int" | "string" | "bool"
// IntLiteral    = `[0-9]+`
// StringLiteral = `"([^\\\n\t]|\\\\|\\n|\\t)*"`
// Ident         = `[a-zA-z][a-zA-z0-9_]*`
public struct Position
{
    public int Line;
    public int Col;
    public override string ToString()
    {
        return string.Format("{0}:{1}", Line, Col);
    }
}
enum Tag
{
    Semicolon,
    Assignment,
    Colon,
    DotDot,
    LParen,
    RParen,
    AddOp,
    SubOp,
    MulOp,
    DivOp,
    LtOp,
    EqOp,
    AndOp,
    NotOp,
    IntLiteral,
    StringLiteral,
    Var,
    For,
    End,
    In,
    Do,
    Read,
    Print,
    Int,
    String,
    Bool,
    Assert,
    Ident,
    Eof,
    Error,
}
class Scanner
{
    public Tag Tag;
    public Position Position;
    public int Int;
    public string String;
    public Scanner(System.IO.TextReader reader)
    {
        _Reader = reader;
        _Sb = new System.Text.StringBuilder();
    }
    public override string ToString()
    {
        String s = string.Format(
            "{0}: {1}",
            Position,
            Tag.ToString());
        if (Tag == Tag.Ident || Tag == Tag.Error)
            return string.Format("{0}: \"{1}\"", s, String);
        else if (Tag == Tag.IntLiteral)
            return string.Format("{0}: {1}", s, Int);
        return s;
    }
    public Tag Scan()
    {
    Again:
        Position.Line = _Line;
        Position.Col = _Col;
        if (_Accept('\0'))
            Tag = Tag.Eof;
        else if (_Accept(';'))
            Tag = Tag.Semicolon;
        else if (_Accept(':'))
        {
            if (_Accept('='))
                Tag = Tag.Assignment;
            else
                Tag = Tag.Colon;
        }
        else if (_Accept('.'))
        {
            if (_Accept('.'))
                Tag = Tag.DotDot;
            else
            {
                Tag = Tag.Error;
                String = "expetced '.'";
            }
        }
        else if (_Accept('('))
            Tag = Tag.LParen;
        else if (_Accept(')'))
            Tag = Tag.RParen;
        else if (_Accept('+'))
            Tag = Tag.AddOp;
        else if (_Accept('-'))
            Tag = Tag.SubOp;
        else if (_Accept('*'))
            Tag = Tag.MulOp;
        else if (_Accept('/'))
        {
            if (_Accept('/'))
            {
                while (_Peek != '\n' && _Peek != '\0')
                    _Read();
                _Accept('\n');
                goto Again;
            }
            else if (_Accept('*'))
            {
                while (_Decline('\n'))
                    if (_Accept('*') && _Accept('/'))
                        goto Again;
                Tag = Tag.Error;
                String = "expected '*/'";
            }
            else
                Tag = Tag.DivOp;
        }
        else if (_Accept('<'))
            Tag = Tag.LtOp;
        else if (_Accept('='))
            Tag = Tag.EqOp;
        else if (_Accept('&'))
            Tag = Tag.AndOp;
        else if (_Accept('!'))
            Tag = Tag.NotOp;
        else if (_Accept('"'))
        {
            _Sb.Clear();
            while (_Peek != '"')
                _Sb.Append(_Read());
            if (_Accept('"'))
            {
                Tag = Tag.StringLiteral;
                String = _Sb.ToString();
                String = String
                    .Replace("\\\\", "\\")
                    .Replace("\\n", "\n")
                    .Replace("\\t", "\t");
            }
            else
            {
                Tag = Tag.Error;
                String = "expected \"";
            }
        }
        else if (Char.IsDigit(_Peek))
        {
            _Sb.Clear();
            do
                _Sb.Append(_Read());
            while (Char.IsDigit(_Peek));
            Tag = Tag.IntLiteral;
            Int = Int32.Parse(_Sb.ToString());
        }
        else if (Char.IsLetter(_Peek))
        {
            _Sb.Clear();
            do
                _Sb.Append(_Read());
            while (Char.IsLetterOrDigit(_Peek) || _Peek == '_');
            string word = _Sb.ToString();
            if (word == "var")
                Tag = Tag.Var;
            else if (word == "for")
                Tag = Tag.For;
            else if (word == "end")
                Tag = Tag.End;
            else if (word == "in")
                Tag = Tag.In;
            else if (word == "do")
                Tag = Tag.Do;
            else if (word == "read")
                Tag = Tag.Read;
            else if (word == "print")
                Tag = Tag.Print;
            else if (word == "int")
                Tag = Tag.Int;
            else if (word == "string")
                Tag = Tag.String;
            else if (word == "bool")
                Tag = Tag.Bool;
            else if (word == "assert")
                Tag = Tag.Assert;
            else
            {
                Tag = Tag.Ident;
                String = word;
            }
        }
        else
        {
            Tag = Tag.Error;
            String = "unknown token";
        }
        return Tag;
    }
    System.IO.TextReader _Reader;
    System.Text.StringBuilder _Sb;
    int _Line = 0;
    int _Col = 0;
    char _Read()
    {
        int c = _Reader.Read();
        if (c == -1)
            return '\0';
        char ret = (char)c;
        if (ret == '\n')
        {
            _Line++;
            _Col = 0;
        }
        else
            _Col++;
        return ret;
    }
    char _Peek
    {
        get
        {
            int c = _Reader.Peek();
            if (c == -1)
                return '\0';
            return (char)c;
        }
    }
    bool _Accept(params char[] expectations)
    {
        _SkipWhiteSpace();
        foreach (char e in expectations)
            if (_Peek == e)
            {
                _Read();
                return true;
            }
        return false;
    }
    bool _Decline(params char[] expectations)
    {
        _SkipWhiteSpace();
        if (_Peek == '\0')
            return false;
        foreach (char e in expectations)
            if (_Peek == e)
                return false;
        _Read();
        return true;
    }
    void _SkipWhiteSpace()
    {
        while (Char.IsWhiteSpace(_Peek))
            _Read();
    }
}
enum VarType
{
    Int,
    String,
    Bool,
}
abstract class Ast
{
    public Position Position;
    public override string ToString()
    {
        return string.Format("{0}({1})", GetType().Name, Position);
    }
}
class StmtList : Ast
{
    public List<Stmt> Stmts;
}
abstract class Stmt : Ast
{
}
class VarStmt : Stmt
{
    public string Ident;
    public VarType Type;
    public Expr Expr;
}
class ForStmt : Stmt
{
    public string Ident;
    public Expr Start;
    public Expr End;
    public StmtList StmtList;
}
class ReadStmt : Stmt
{
    public string Ident;
}
class PrintStmt : Stmt
{
    public Expr Expr;
}
class AssertStmt : Stmt
{
    public Expr Expr;
}
class AssignmentStmt : Stmt
{
    public string Ident;
    public Expr Expr;
}
abstract class Expr : Ast
{
}
class IntExpr : Expr
{
    public int Value;
    public string AsString()
    {
        return Value.ToString();
    }
}
class StringExpr : Expr
{
    public string Value;
    public string AsString()
    {
        return Value;
    }
}
class BoolExpr : Expr
{
    public bool Value;
    public string AsString()
    {
        return Convert.ToString(Value);
    }
}
class IdentExpr : Expr
{
    public string Ident;
}
class NotExpr : Expr
{
    public Expr Expr;
}
class BinExpr : Expr
{
    public Tag Op;
    public Expr Lhs;
    public Expr Rhs;
}
class Parser
{
    public Parser(Scanner scanner)
    {
        _Scanner = scanner;
        _Scanner.Scan();
    }
    // Program = Stmts Eof
    public StmtList Parse()
    {
        StmtList ret = new StmtList{ Position = _Pos };
        ret.Stmts = _Stmts();
        _Require(Tag.Eof);
        return ret;
    }
    Scanner _Scanner;
    Tag _Peek
    {
        get => _Scanner.Tag;
    }
    Position _Pos
    {
        get => _Scanner.Position;
    }
    // Stmts = (Stmt ";")+
    // FOLLOW: End | Eof
    List<Stmt> _Stmts()
    {
        List<Stmt> ret = new List<Stmt>();
        bool parseFailure = false;
        while (!_Next(Tag.End, Tag.Eof))
            try
            {
                Stmt stmt = _Stmt();
                _Require(Tag.Semicolon);
                ret.Add(stmt);
            }
            catch (ParseException e)
            {
                // panic mode
                Console.WriteLine("PANIC: {0}:\n    {1}",
                                  e.GetType().Name,
                                  e.Message);
                while (_Decline(Tag.Var,
                                Tag.For,
                                Tag.Read,
                                Tag.Print,
                                Tag.Assert,
                                Tag.Ident)) ;
                Console.WriteLine("skip to: " + _Scanner);
                parseFailure = true;
            }
        if (parseFailure)
            throw new ParseException(
                string.Format(
                    "{0}:{1}: parser panicked",
                    _Scanner.Position.Line,
                    _Scanner.Position.Col));
        return ret;
    }
    // Stmt = "var" Ident ":" Type [":=" Expr]
    //      = "for" Ident "in" Expr ".." Expr "do" Stmts "end" "for"
    //      = "read" Ident
    //      = "print" Expr
    //      = "assert" "(" Expr ")"
    //      = Ident ":=" Expr
    Stmt _Stmt()
    {
        if (_Accept(Tag.Var))
        {
            VarStmt ret = new VarStmt{ Position = _Pos };
            ret.Ident = _Ident();
            _Require(Tag.Colon);
            ret.Type = _Type();
            if (_Accept(Tag.Assignment))
                ret.Expr = _Expr();
            return ret;
        }
        else if (_Accept(Tag.For))
        {
            ForStmt ret = new ForStmt{ Position = _Pos };
            ret.Ident = _Ident();
            _Require(Tag.In);
            ret.Start = _Expr();
            _Require(Tag.DotDot);
            ret.End = _Expr();
            _Require(Tag.Do);
            ret.StmtList = new StmtList { Position = _Pos };
            ret.StmtList.Stmts = _Stmts();
            _Require(Tag.End);
            _Require(Tag.For);
            return ret;
        }
        else if (_Accept(Tag.Read))
        {
            ReadStmt ret = new ReadStmt{ Position = _Pos };
            ret.Ident = _Ident();
            return ret;
        }
        else if (_Accept(Tag.Print))
        {
            PrintStmt ret = new PrintStmt{ Position = _Pos };
            ret.Expr = _Expr();
            return ret;
        }
        else if (_Accept(Tag.Assert))
        {
            AssertStmt ret = new AssertStmt{ Position = _Pos };
            _Require(Tag.LParen);
            ret.Expr = _Expr();
            _Require(Tag.RParen);
            return ret;
        }
        else if (_Next(Tag.Ident))
        {
            AssignmentStmt ret = new AssignmentStmt{ Position = _Pos };
            ret.Ident = _Ident();
            _Require(Tag.Assignment);
            ret.Expr = _Expr();
            return ret;
        }
        throw _Error();
    }
    // Expr = "!" Opnd
    //      = Opnd [BinOp Opnd]
    Expr _Expr()
    {
        if (_Accept(Tag.NotOp))
        {
            NotExpr ret = new NotExpr{ Position = _Pos };
            ret.Expr = _Opnd();
            return ret;
        }
        else if (_Next(Tag.IntLiteral, Tag.StringLiteral, Tag.Ident))
        {
            Expr lhs = _Opnd();
            if (_Next(Tag.AddOp,
                      Tag.SubOp,
                      Tag.MulOp,
                      Tag.DivOp,
                      Tag.LtOp,
                      Tag.EqOp,
                      Tag.AndOp))
            {
                BinExpr ret = new BinExpr{ Position = _Pos };
                ret.Lhs = lhs;
                ret.Op = _BinOp();
                ret.Rhs = _Opnd();
                return ret;
            }
            return lhs;
        }
        throw _Error();
    }
    // Opnd = IntLiteral
    //      = StringLiteral
    //      = Ident
    //      = "(" Expr ")"
    Expr _Opnd()
    {
        if (_Accept(Tag.IntLiteral))
        {
            IntExpr ret = new IntExpr{ Position = _Pos };
            ret.Value = _Scanner.Int;
            return ret;
        }
        else if (_Accept(Tag.StringLiteral))
        {
            StringExpr ret = new StringExpr{ Position = _Pos };
            ret.Value = _Scanner.String;
            return ret;
        }
        else if (_Accept(Tag.Ident))
        {
            IdentExpr ret = new IdentExpr{ Position = _Pos };
            ret.Ident = _Scanner.String;
            return ret;
        }
        else if (_Accept(Tag.LParen))
        {
            Expr ret = _Expr();
            _Require(Tag.RParen);
            return ret;
        }
        throw _Error();
    }
    // BinOp = "+" | "-" | "*" | "/" | "<" | "=" | "&"
    Tag _BinOp()
    {
        if (_Accept(Tag.AddOp))
            return Tag.AddOp;
        else if (_Accept(Tag.SubOp))
            return Tag.SubOp;
        else if (_Accept(Tag.MulOp))
            return Tag.MulOp;
        else if (_Accept(Tag.DivOp))
            return Tag.DivOp;
        else if (_Accept(Tag.LtOp))
            return Tag.LtOp;
        else if (_Accept(Tag.EqOp))
            return Tag.EqOp;
        else if (_Accept(Tag.AndOp))
            return Tag.AndOp;
        throw _Error();
    }
    // Type = "int" | "string" | "bool"
    VarType _Type()
    {
        if (_Accept(Tag.Int))
            return VarType.Int;
        else if (_Accept(Tag.String))
            return VarType.String;
        else if (_Accept(Tag.Bool))
            return VarType.Bool;
        throw _Error();
    }
    // Ident = `[a-zA-z][a-zA-z0-9_]*`
    string _Ident()
    {
        if (_Accept(Tag.Ident))
            return _Scanner.String;
        throw _Error();
    }
    Tag _Scan()
    {
        return _Scanner.Scan();
    }
    bool _Next(params Tag[] expectations)
    {
        foreach (Tag e in expectations)
            if (_Peek == e)
                return true;
        return false;
    }
    bool _Accept(params Tag[] expectations)
    {
        if (_Next(expectations))
        {
            Console.WriteLine("accept: " + _Scanner);
            _Scan();
            return true;
        }
        return false;
    }
    void _Require(Tag expectation)
    {
        if (!_Accept(expectation))
            throw _Error(expectation);
    }
    bool _Decline(params Tag[] expectations)
    {
        if (_Peek == Tag.Eof)
            return false;
        foreach (Tag e in expectations)
            if (_Peek == e)
                return false;
        Console.WriteLine("decline: " + _Scanner);
        _Scan();
        return true;
    }
    ParseException _Error()
    {
        return new ParseException(
            string.Format(
                "{0}: parse error: function: {1}, token: {2}",
                _Scanner.Position,
                new StackFrame(1, true).GetMethod().Name,
                _Scanner));
    }
    ParseException _Error(Tag expected = Tag.Error)
    {
        return new ParseException(
            string.Format(
                "{0}: parse error: function: {1}, token: {2}, expected: {3}",
                _Scanner.Position,
                new StackFrame(1, true).GetMethod().Name,
                _Scanner,
                expected));
    }
}
class AstPrinter
{
    public void Print(Ast ast)
    {
        Print(ast as dynamic);
    }
    public void Print(StmtList ast)
    {
        foreach (Stmt stmt in ast.Stmts)
            Print(stmt);
    }
    public void Print(VarStmt ast)
    {
        TreePrint(ast);
        if (ast.Expr != null)
            Print(ast.Expr);
    }
    public void Print(AssignmentStmt ast)
    {
        TreePrint(ast);
        Print(new IdentExpr { Ident = ast.Ident });
        Print(ast.Expr);
    }
    public void Print(ReadStmt ast)
    {
        TreePrint(ast);
        Print(new IdentExpr { Ident = ast.Ident });
    }
    public void Print(PrintStmt ast)
    {
        TreePrint(ast);
        Print(ast.Expr);
    }
    public void Print(AssertStmt ast)
    {
        TreePrint(ast);
        Print(ast.Expr);
    }
    public void Print(ForStmt ast)
    {
        TreePrint(ast);
        Print(new IdentExpr { Ident = ast.Ident });
        Print(ast.Start);
        Print(ast.End);
        TreePrint("{");
        Depth+=2;
        Print(ast.StmtList);
        Depth-=2;
        TreePrint("}");
    }
    public void Print(IntExpr ast)
    {
        TreePrint(ast);
    }
    public void Print(StringExpr ast)
    {
        TreePrint(ast);
    }
    public void Print(BoolExpr ast)
    {
        TreePrint(ast);
    }
    public void Print(IdentExpr ast)
    {
        TreePrint(ast);
    }
    public void Print(NotExpr ast)
    {
        TreePrint(ast);
    }
    public void Print(BinExpr ast)
    {
        TreePrint(ast);
        Depth++;
        Print(ast.Lhs);
        Print(ast.Rhs);
        Depth--;
    }
    int Depth = 0;
    void TreePrint(Ast ast)
    {
        TreePrint(ast as dynamic);
    }
    void TreePrint(Stmt ast)
    {
        Console.WriteLine("{0}{1}", new String(' ', Depth * 4), ast);
    }
    void TreePrint(Expr ast)
    {
        Console.WriteLine("{0}{1}",
                          new String(' ', (Depth + 1) * 4), ast);
    }
    void TreePrint(string s)
    {
        Console.WriteLine("{0}{1}", new String(' ', (Depth + 1) * 4), s);
    }
}
class AstSemanticAnalyzer
{
    public void Analyze(StmtList ast)
    {
        foreach (Stmt stmt in ast.Stmts)
            AnalyzeStmt(stmt);
    }
    public void AnalyzeStmt(Stmt ast)
    {
        AnalyzeStmt(ast as dynamic);
    }
    public void AnalyzeStmt(VarStmt ast)
    {
        if(ast.Expr != null && ast.Type != Type(ast.Expr))
            throw new TypeException(ast, ast.Type);
        _SymTab[ast.Ident] = ast.Type;
    }
    public void AnalyzeStmt(AssignmentStmt ast)
    {
        if(Type(ast.Ident) != Type(ast.Expr))
            throw new TypeException(ast);
    }
    public void AnalyzeStmt(ReadStmt ast)
    {
        VarType type = Type(ast.Ident);
        if(type != VarType.Int &&
           type != VarType.String &&
           type != VarType.Bool)
            throw new TypeException(ast);
    }
    public void AnalyzeStmt(PrintStmt ast)
    {
        VarType type = Type(ast.Expr);
        if(type != VarType.Int &&
           type != VarType.String)
            throw new TypeException(ast);
    }
    public void AnalyzeStmt(ForStmt ast)
    {
        if(Type(ast.Start) != VarType.Int ||
           Type(ast.End) != VarType.Int)
            throw new TypeException(ast, VarType.Int);
        Analyze(ast.StmtList);
    }
    public void AnalyzeStmt(AssertStmt ast)
    {
        if(Type(ast.Expr) != VarType.Bool)
            throw new TypeException(ast, VarType.Bool);
    }
    public VarType Type(Expr ast)
    {
        return Type(ast as dynamic);
    }
    public VarType Type(IntExpr ast)
    {
        return VarType.Int;
    }
    public VarType Type(StringExpr ast)
    {
        return VarType.String;
    }
    public VarType Type(BoolExpr ast)
    {
        return VarType.Bool;
    }
    public VarType Type(IdentExpr ast)
    {
        return Type(ast.Ident);
    }
    public VarType Type(string ident)
    {
        return _SymTab[ident];
    }
    public VarType Type(BinExpr ast)
    {
        VarType lht = Type(ast.Lhs);
        VarType rht = Type(ast.Rhs);
        if (lht == VarType.Int &&
            rht == VarType.Int &&
            (ast.Op == Tag.AddOp ||
             ast.Op == Tag.SubOp ||
             ast.Op == Tag.MulOp ||
             ast.Op == Tag.DivOp))
            return VarType.Int;
        else if (lht == VarType.String &&
                 rht == VarType.String &&
                 ast.Op == Tag.AddOp)
            return VarType.String;
        else if (lht == VarType.Bool &&
                 rht == VarType.Bool &&
                 ast.Op == Tag.AndOp ||
                 (lht == rht && (ast.Op == Tag.EqOp || ast.Op == Tag.LtOp)))
            return VarType.Bool;
        throw new TypeException(ast);
    }
    public VarType Type(NotExpr ast)
    {
        if(Type(ast.Expr) != VarType.Bool)
            throw new TypeException(ast, VarType.Bool);
        return VarType.Bool;
    }
    Dictionary<string, VarType> _SymTab = new Dictionary<string, VarType>();
}
class AstEvaluator
{
    public void Eval(StmtList ast)
    {
        foreach (Stmt stmt in ast.Stmts)
            EvalStmt(stmt);
    }
    public void EvalStmt(Stmt ast)
    {
        EvalStmt(ast as dynamic);
    }
    public void EvalStmt(VarStmt ast)
    {
        if (ast.Expr != null)
            _SymTab[ast.Ident] = EvalExpr(ast.Expr);
        else
            if (ast.Type == VarType.Int)
                _SymTab[ast.Ident] = new IntExpr();
            else if (ast.Type == VarType.String)
                _SymTab[ast.Ident] = new StringExpr();
            else if (ast.Type == VarType.Bool)
                _SymTab[ast.Ident] = new BoolExpr();
            else
                throw new EvalException(ast);
    }
    public void EvalStmt(AssignmentStmt ast)
    {
        _SymTab[ast.Ident] = EvalExpr(ast.Expr);
    }
    public void EvalStmt(ReadStmt ast)
    {
        string line = Console.ReadLine();
        if (Type(ast.Ident) == VarType.Int)
            _SymTab[ast.Ident] = new IntExpr { Value = Int32.Parse(line) };
        else  if (Type(ast.Ident) == VarType.String)
            _SymTab[ast.Ident] = new StringExpr { Value = line };
        else
            throw new EvalException(ast);
    }
    public void EvalStmt(PrintStmt ast)
    {
        Console.Write((EvalExpr(ast.Expr) as dynamic).AsString());
    }
    public void EvalStmt(ForStmt ast)
    {
        IntExpr start = EvalExpr(ast.Start) as IntExpr;
        IntExpr end = EvalExpr(ast.End) as IntExpr;
        IntExpr index = new IntExpr{ Value = start.Value };
        _SymTab[ast.Ident] = index;
        for (; index.Value <= end.Value; index.Value++)
            Eval(ast.StmtList);
    }
    public void EvalStmt(AssertStmt ast)
    {
        BoolExpr cond = EvalExpr(ast.Expr) as BoolExpr;
        if(!cond.Value)
            Console.WriteLine("Assertion failed: {0}", ast.Expr);
    }
    public Expr EvalExpr(Expr ast)
    {
        return EvalExpr(ast as dynamic);
    }
    public Expr EvalExpr(IntExpr ast)
    {
        return new IntExpr { Value = ast.Value };
    }
    public Expr EvalExpr(StringExpr ast)
    {
        return new StringExpr { Value = ast.Value };
    }
    public Expr EvalExpr(BoolExpr ast)
    {
        return new BoolExpr { Value = ast.Value };
    }
    public Expr EvalExpr(IdentExpr ast)
    {
        var value = (_SymTab[ast.Ident] as dynamic).Value;
        VarType type = Type(ast);
        if (type == VarType.Int)
            return new IntExpr { Value = value };
        else if (type == VarType.String)
            return new StringExpr { Value = value };
        else if (type == VarType.Bool)
            return new BoolExpr { Value = value };
        throw new EvalException(ast);
    }
    public Expr EvalExpr(BinExpr ast)
    {
        Expr lhe = EvalExpr(ast.Lhs);
        Expr rhe = EvalExpr(ast.Rhs);
        VarType lht = Type(lhe);
        VarType rht = Type(rhe);
        var lhv = (lhe as dynamic).Value;
        var rhv = (rhe as dynamic).Value;
        if (ast.Op == Tag.AddOp)
            if (lht == VarType.Int)
                return new IntExpr { Value = lhv + rhv };
            else
                return new StringExpr { Value = lhv + rhv };
        else if (ast.Op == Tag.SubOp)
            return new IntExpr { Value = lhv - rhv };
        else if (ast.Op == Tag.MulOp)
            return new IntExpr { Value = lhv * rhv };
        else if (ast.Op == Tag.DivOp)
            return new IntExpr { Value = lhv / rhv };
        else if (ast.Op == Tag.AndOp)
            return new BoolExpr { Value = lhv && rhv };
        else if (ast.Op == Tag.EqOp)
            return new BoolExpr { Value = lhv == rhv };
        else
            return new BoolExpr { Value = lhv < rhv };
        throw new EvalException(ast);
    }
    public Expr EvalExpr(NotExpr ast)
    {
        BoolExpr arg = EvalExpr(ast.Expr) as BoolExpr;
        return new BoolExpr{Value = !arg.Value};
    }
    public VarType Type(Expr ast)
    {
        return Type(EvalExpr(ast) as dynamic);
    }
    public VarType Type(IntExpr ast)
    {
        return VarType.Int;
    }
    public VarType Type(StringExpr ast)
    {
        return VarType.String;
    }
    public VarType Type(BoolExpr ast)
    {
        return VarType.Bool;
    }
    public VarType Type(IdentExpr ast)
    {
        return Type(ast.Ident);
    }
    public VarType Type(string ident)
    {
        return Type(_SymTab[ident]);
    }
    Dictionary<string, Expr> _SymTab = new Dictionary<string, Expr>();
}
class CompilerException : Exception
{
    public CompilerException(string message) : base(message) { }
}
class ParseException : CompilerException
{
    public ParseException(string message) : base(message) { }
}
class TypeException : CompilerException
{
    public TypeException(Ast ast) :
    base(string.Format("wrong type: {0}", ast)) { }
    public TypeException(Ast ast, VarType expected) :
    base(string.Format("wrong type: {0}, expected: {1}", ast, expected)) { }
}
class EvalException : CompilerException
{
    public EvalException(Ast ast) : base(ast.ToString()) { }
}
class MainClass
{
    static void EvalFile(string path)
    {
        try
        {
            string text = System.IO.File.ReadAllText(path);
            Console.WriteLine("------------------------------------------");
            Console.WriteLine("Parser:");
            Console.WriteLine("------------------------------------------");
            Scanner scanner = new Scanner(new System.IO.StringReader(text));
            Parser parser = new Parser(scanner);
            StmtList stmtList = parser.Parse();
            Console.WriteLine("------------------------------------------");
            Console.WriteLine("Syntax tree:");
            Console.WriteLine("------------------------------------------");
            new AstPrinter().Print(stmtList);
            Console.WriteLine("------------------------------------------");
            Console.Write("Semantic analyzer: ");
            new AstSemanticAnalyzer().Analyze(stmtList);
            Console.WriteLine("OK");
            Console.WriteLine("------------------------------------------");
            Console.WriteLine("Interpreter:");
            Console.WriteLine("------------------------------------------");
            new AstEvaluator().Eval(stmtList);
            Console.WriteLine();
            Console.WriteLine("------------------------------------------");
        }
        catch (CompilerException e)
        {
            Console.WriteLine("ERROR: {0}: {1}", e.GetType().Name, e.Message);
        }
    }
    static void Main(string[] args)
    {
        if (args.Length != 1)
            Console.WriteLine(
                "usage: {0} filename",
                System.Diagnostics.Process.GetCurrentProcess().ProcessName);
        else
            EvalFile(args[0]);
    }
}
}
