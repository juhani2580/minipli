var i1 : int := 1;
var i2 : int := 2;
var i3 : int := 3;
var i4 : int := 4;
var i5 : int := 5;
print i1 + (i2 + i3); // 6
print "\n";
print i1 + (i4 - i2); // 3
print "\n";
print i1 + (i2 * i3); // 7
print "\n";
print i1 + (i5 / i2); // 3
print "\n";
var s1 : string := "foo";
var s2 : string := "bar";
print s1 + s2; // foobar
print "\n";
var b1 : bool := 1 = 1;
var b2 : bool := 1 = 0;
assert(b1);
assert(!b2);