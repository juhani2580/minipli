var a : int := 5;
print a; // 5
print "\n";
a := 7;
print a; // 7
print "\n";
var b : string := "hello world";
print b; // hello world
print "\n";
b := "foobar";
print b; // foobar
print "\n";
var c : bool := 5 = 5;
assert(c);
assert(!(!c));
var d : int;
print "Give an integer:\n";
read d;
print d;
print "\n";
var e : string;
print "Give a string:\n";
read e;
print e;
print "\n";
print "\n";
var nTimes : int;
print "How many times?\n";
read nTimes;
var x : int;
for x in 0..nTimes-1 do
    print x;
    print " : Looping is fun!\n";
end for;
assert (x = nTimes);
print "BYE!\n";
